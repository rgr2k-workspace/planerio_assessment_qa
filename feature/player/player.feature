Feature: Player CRUD

   As a restFootball API user I want to be able to execute the four basic operation
   so as I can check whether the system meets the defined constraints and requirements or not

   Scenario: User should be able to retrieve all player

   Given The user is on the system
   When  The user requests a list of users
   Then  A list with all the registered players should be retrivied.

   Scenario: User should be able to retrieve a specific player by id
   
   Given The user is on the system
   When  The user requests information about a specific player by passing his id number
   Then  The information about the player should be retrieved

   Scenario: User should be able to retrieve a list of players by a Team name
   
   Given The user is on the system
   When  The user requests information about players by passing a Team name
   Then  A list with all the registered players of the Team name provided should be retrivied.

   Scenario: User wants to know who are the player who plays in Defense Position
   
   Given The user is on the system
   When  The user requests information about players who plays in Defense position
   Then  A list with the player who plays in the defense should be retrieved

   Scenario: User wants to know who are the player for a specific position
   
   Given The user is on the system
   When  The user requests information about players who plays in a specific position
   Then  A list with the player who plays in the given position should be retrieved

   Scenario: User requests to get an invalid player by Id
   
   Given The user is on the system
   When  The user requests information about a player who doesn't exists by passing an invalid id
   Then  A 404 Not Found Exception message is displayed to the user

   Scenario: User requests to get an invalid player by Name
   
   Given The user is on the system
   When  The user requests information about a player who doesn't exists by passing an invalid name
   Then  A 404 Not Found Exception message is displayed to the user

   Scenario: User creates a player

   Given The user is on the system
   When  The user sets all player information
   And   The user sends the player information to the system
   Then  A successfull message should be displayed with the newly player information

   Scenario: User tries to create a player with a already set Jersey Number

   Given The user is on the system
   When  The user sets all player information and the Jersey Number <11>
   And   The user sends the player information to the system
   Then  A Bad Request message should be displayed informing that the Jersey number already exists

   Scenario: User tries to create a player with a out of range Jersey Number

   Given The user is on the system
   And   The range of Jersey Number are from 1 to 99
   When  The user sets all player information with invalid Jersey Number <100>
   And   The user sends the player information to the system
   Then  A Bad Request message should be displayed informing that the Jersey number is outside of the range

   Scenario: User tries to create a player in a team which already has more than 25 players set

   Given The user is on the system
   And   The Limit number of players in a team is defined
   When  The user sets all player information
   And   The user sends the player information to the system
   Then  A Bad Request message should be displayed informing that There cannot be more than 25 players in a team

   Scenario: User updates a player information

   Given The user is on the system
   And   A given player already exists
   When  The user sets all player new and updated data
   And   The user sends the player information to the system
   Then  The player data should be updated successfully

   Scenario: User tries to update a player that doesn't exists

   Given The user is on the system
   When  The user sets all invalid player new and updated data
   And   The user sends the player information to the system with a invalid id
   Then  A 404 Not Found Exception message is displayed to the user as Id not found

   Scenario: User deletes a player

   Given The user is on the system
   And   A given player exists
   When  The user sends the player id to be deleted
   Then  The player is successfully removed from the system

 Scenario: User tries to delete a player that doesn't exists

   Given The user is on the system
   When  The user sets an invalid player id
   And   The user sends the player information to the system with an invalid id
   Then  A 404 Not Found Exception message is displayed to the user as Id not found