# RestFootball QA Technical Test

Hi! I'm Rodrigo Rodrigues and I will try to help you to evaluate my Technical Test for Planerio's QA Tester vacancy. This document is divided in some sections to facilitate find where it's distributed the aspects of the assessment.

## Test Plan & Test Strategy

The **Test Plan** and the **Test Strategy** is placed inside the folder: **/testplan** 

I have proposed a general Test Plan along with a Test Strategy because it's the way I'm used to work.
It looks like pretty formal, but of course I would adapt those documents as much as possible according the Planerio's style and processes.

As part of the Test Plan, I have also take the **/player** endpoint and included a **/feature** **folder** where you can check the **CRUD** and some constraints test cases for /Player endpoint in the Gherkin Language (Given When Then)
Ps: I would describe and write the test cases using this style with more time and of course linking this to the Automation Tool, so it would provided more value to the stakeholders.

## API Tests cases, Mocks and Stubs

The Postman Collection which contains the TestCases is placed inside the folder: **/api** 
You can also try to import the Collection.json from this [link:](https://www.getpostman.com/collections/edbf8c71b2f95cde1dff)

I decided to use Postman tool to implement the test cases and by using mocks and stubs to represents all the resources listed in the questionary. I think that by doing this would make easier to understand and check how would be my approach in terms of organization, contracting thinking, assertions and so on.

Of course there's room to improve this and use other tools that Planerio consider handy and with hight level of productivity.

Despite Postman, to API Test Automation I have also worked with: restAssured with Java, wiremock to mock and stubbing and soapUI.

I have also published the documentation of the Test cases and you can preview this by clicking on this url
[https://documenter.getpostman.com/view/11213695/SzfCSk5h?version=latest](https://documenter.getpostman.com/view/11213695/SzfCSk5h?version=latest)

If you have the last version of Postman installed and click on Run in the page above, I think it will work.

## Answering the Questionary

**1. Develop a test plan for this API** -> is placed inside the folder: **/testplan** 

**2. How would you make sure changes to the API keep working functionality intact?**

	I would perform a Regression Test following the test plan and make sure that the entire API was not impacted with the changes.  
	The ideal is to have a Test Automation suite to run after every change in the API.
	There's a section on Test Strategy where I mention Regression Testing.  

**3. Suppose that there is a bug in the “Retrieve the list of all players who play in defense” feature. You can assume the bug is located in PlayerService.java. How would you write the ticket describing this bug? Provide an example title and description, and anything else you would add to the ticket.**

**Summary:** GET /players is retrieving more positions than defense positions

**Description:** When a user requests for players who plays only in defense, it's also returning 'CF' positions.

**Steps to Reproduce:**

 1. Run the GET request api/v1/player?positions=GK,CB,RB,LB,LWB,RWB&sort=name,desc
 2. Check that in the response body it also contains two player which positions are CF

**Expected Result:**  The request should only return players related to the positions: { "GK", "CB", "RB", "LB", "LWB", "RWB"}

**Severity:** High

**Priority:** High

**Environment:** http://3ac597f1-aab1-461f-a833-e8887f98f035.mock.pstmn.io/player

**Version:** v1.0

**Screenshot:** (if applicable)

**Bonus question: Say you have a backlog with 391 bugs. What are the steps you would follow to groom this backlog and reduce the amount of bugs?**


	In this case, it's clear that it's impossible to prioritize and fix such number of bugs. What I would propose with the development team along
	with the Product Owner is to re-check the reported bugs and try to answer some questions like:
	- How serious is the bug? - How often does the bug occur? - Bug detrects the company's image?
	Ultimately it's a priorization decision, so the Team alongwith the  Product Owner and Stakeholders should also participate in this decision.
